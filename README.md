# INLEO

This GitLab is used to share, discuss and collaborate on the LEO Token Economy, INLEO Platform and LeoDex Exchange Interface. The team and community post ideas, roadmap features, etc. and collaborate to improve them as they are rolled out to production.

## About

INLEO is building a thriving creator economy that is centered around digital ownership, tokenization and communities. Our flagship application (inleo.io) features a "twitter-like" feeling on Web3. The underlying architecture is built entirely on a blockchain, providing users with immutable digital identities.

**What is an immutable digital identity?**

It's a profile that can't be taken away, banned or otherwise shut down. This allows a supercharging of the consumer-creator experience to socialize like never before.

INLEO offers boundless opportunities whether one is:

- looking to monetize content
- build a personal or corporate brand
- consume content
- earn rewards 

INLEO aims to be the hub for the next generation of social activities.

## Digital Identity

Web3 is often misconstrued with NFTs, hype-cycles and other problematic "trends". To us, Web3 represents a new layer of the internet that democratizes ownership. 

Web2 is commonly referred to as the "social media era" of the internet. In Web2, you have little control over your identity on the internet. Whether we accept it or not, we all now have a digital identity and a physical identity. Your digital self lives in all the data, consumption and interaction you have online. Companies have learned how to use this to build multi-billion dollar organizations through productizing user-generated content. 
INLEO takes this model and rebuilds it at a fundamental level. Instead of a central corporation owning your data and profiting from your content, INLEO enables you to truly own your digital identity and benefit from the attention economy that is powered by creators on inleo.io. Monetizing content and platform ownership is just one piece of the puzzle. INLEO enables creators to own the entire stack through blockchain technology. 